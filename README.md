## Metric to Imperial Converter


*** Conversion of different units

##To Run

```bash
install java v17+
install maven v3
```

Use Docker command:

```bash
mvn clean package
docker build --build-arg JAR_FILE="target/*.jar" -t <image-name> .
docker run -p 8080:8080 <image-name>
```

---
Copyright © 2021. Using this code or part of it in any way without permission is prohibited!
