package co.za.example.service;

import co.za.example.error.InvalidConvensionException;

/**
 *
 * @date 2021/11/27
 */
public class TemperatureConverter extends BaseConverter {

    @Override
    public double convert (String fromUnit, String toUnit, final double value) {
        System.out.println("======================= temperature conversion ===============");

        if (fromUnit.equalsIgnoreCase("c") && toUnit.equalsIgnoreCase("f"))
            return fromCelsiusToFahrenheit(value);
        if (fromUnit.equalsIgnoreCase("c") && toUnit.equalsIgnoreCase("c"))
            return fromCelsiusToCelsius(value);
        if (fromUnit.equalsIgnoreCase("c") && toUnit.equalsIgnoreCase("k"))
            return fromCelsiusToKelvin(value);
        if (fromUnit.equalsIgnoreCase("f") && toUnit.equalsIgnoreCase("c"))
            return fromFahrenheitToCelsius(value);
        if (fromUnit.equalsIgnoreCase("f") && toUnit.equalsIgnoreCase("f"))
            return fromFahrenheitToFahrenheit(value);
        if (fromUnit.equalsIgnoreCase("f") && toUnit.equalsIgnoreCase("k"))
            return fromFahrenheitToKelvin(value);
        if (fromUnit.equalsIgnoreCase("k") && toUnit.equalsIgnoreCase("c"))
            return fromKelvinToCelsius(value);
        if (fromUnit.equalsIgnoreCase("k") && toUnit.equalsIgnoreCase("k"))
            return fromKelvinToKelvin(value);
        if (fromUnit.equalsIgnoreCase("k") && toUnit.equalsIgnoreCase("f"))
            return fromKelvinToFahrenheit(value);

        throw new InvalidConvensionException("Invalid conversion");
    }

    private double fromCelsiusToFahrenheit(double value) { //
        return (( 9 * value) / 5 ) + 32;
    }

    private double fromCelsiusToCelsius(double value) { //
        return value;
    }

    private double fromCelsiusToKelvin(double value) { //
        return value + 273.15;
    }

    private double fromFahrenheitToCelsius(double value) { //
        return (value - 32) * 5/9;
    }

    private double fromFahrenheitToFahrenheit(double value) { //
        return value;
    }

    private double fromFahrenheitToKelvin(double value) { //
        return  273.5 + ((value - 32.0) * (5.0/9.0));
    }

    private double fromKelvinToCelsius(double value) {
        return value - 273.15F;
    }

    private double fromKelvinToKelvin(double value) {
        return value;
    }

    private double fromKelvinToFahrenheit(double value) {
        return (value * 9/5) - 459.67;
    }
}
