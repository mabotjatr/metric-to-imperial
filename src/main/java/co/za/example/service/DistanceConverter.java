package co.za.example.service;

import co.za.example.error.InvalidConvensionException;

/**
 *
 * @date 2021/11/27
 */
public class DistanceConverter extends BaseConverter {

    private double milesToKm(double value) {
        return value * 1.60934;
    }

    private double kmToMiles(double value) {
        return value * 0.621371;
    }

    private double meterToKilometer(double value) {
        return  value * .001;
    }

    @Override
    public double convert(String fromUnit, String toUnit, double value) {
        System.out.println("======================= Distance conversion ===============");
        if (fromUnit.equalsIgnoreCase("m") && toUnit.equalsIgnoreCase("km"))
            return meterToKilometer(value);
        if (fromUnit.equalsIgnoreCase("km") && toUnit.equalsIgnoreCase("mi"))
            return kmToMiles(value);
        if (fromUnit.equalsIgnoreCase("mi") && toUnit.equalsIgnoreCase("km"))
            return milesToKm(value);

        throw new InvalidConvensionException("Invalid conversion unit passed "+fromUnit);
    }
}
