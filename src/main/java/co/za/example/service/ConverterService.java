package co.za.example.service;

import co.za.example.model.ConvertedDTO;
import co.za.example.model.ConverterDTO;
import org.springframework.stereotype.Service;

/**
 *
 * @date 2021/11/27
 */
@Service
public class ConverterService {

    public ConvertedDTO convertUnits (ConverterDTO converterDTO) {
        var results = ConvertedDTO.builder().build();
        BaseConverter converter;
        if (converterDTO.getMeasurement().equalsIgnoreCase("distance")) {
            converter = new DistanceConverter();
            var value = converter.convert(converterDTO.getFromUnit(),
                    converterDTO.getToUnit(), converterDTO.getValue());

            results.setResult(value);
        } else if (converterDTO.getMeasurement().equalsIgnoreCase("temperature")) {
            converter = new TemperatureConverter();
            var value = converter.convert(converterDTO.getFromUnit(),
                    converterDTO.getToUnit(), converterDTO.getValue());

            results.setResult(value);
        }
        return results;
    }

}
