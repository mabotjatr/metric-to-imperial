package co.za.example.service;

/**
 *
 * @date 2021/11/27
 */
public abstract class BaseConverter {

    /**
     * Method to convert units
     * @param fromUnit the unit from
     * @param toUnit the unit to convert to
     * @param value the value to conver
     * @return the unit desired.
     */
    abstract double convert (String fromUnit, String toUnit, final double value);
}
