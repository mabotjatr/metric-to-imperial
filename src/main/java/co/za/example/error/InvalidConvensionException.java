package co.za.example.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @date 2021/11/27
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidConvensionException extends RuntimeException {

    private String message;

    public InvalidConvensionException (String message) {
        super(message);
        this.message = message;
    }

    public InvalidConvensionException () {
    }
}
