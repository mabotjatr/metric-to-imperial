package co.za.example.model;

import lombok.*;

import java.io.Serializable;

/**
 *
 * @date 2021/11/27
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ConverterDTO implements Serializable {

    private String measurement;

    private String fromUnit;

    private String toUnit;

    private double value;

}
