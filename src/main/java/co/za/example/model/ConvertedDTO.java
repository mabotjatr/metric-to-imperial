package co.za.example.model;

import lombok.*;

import java.io.Serializable;

/**
 * 
 * @date 2021/11/27
 */
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class ConvertedDTO implements Serializable {

    private double result;
}
