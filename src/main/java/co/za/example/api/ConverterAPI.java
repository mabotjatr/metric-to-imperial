package co.za.example.api;

import co.za.example.model.ConvertedDTO;
import co.za.example.model.ConverterDTO;
import co.za.example.service.ConverterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @date 2021/11/27
 */
@RestController
@RequestMapping("/api/v1")
public class ConverterAPI {

    @Autowired
    private ConverterService service;

    /**
     * Convert unit
     *
     * @param converterDTO conversion details
     * @return the converted value
     */
    @PostMapping("convert")
    public ConvertedDTO convert( @RequestBody ConverterDTO converterDTO) {
        return service.convertUnits (converterDTO);
    }

    @GetMapping("convert/{from}/{to}/{measurement}")
    public ConvertedDTO convert(@PathVariable String from, @PathVariable String to,
                                @PathVariable String measurement) {
        var dto = ConverterDTO.builder()
                .fromUnit(from)
                .measurement(measurement)
                .toUnit(to)
                .value(14.36)
                .build();
        return service.convertUnits (dto);
    }
}
